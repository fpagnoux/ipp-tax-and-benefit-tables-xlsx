# Calendrier indicatif des publications et décisions de modifications des paramètres de la législations socio-fiscale

## 1er janvier

### Réunion du conseil d'administration de l'[AGS] (http://www.ags-garantie-salaires.org/archives.html) généralement doublée d'une [circulaire Unedic] (http://www.unedic.org/article/circulaires-de-l-unedic):
 - [ ] prélévements sociaux: AGS
 
### Circulaire [AGIRC-ARRCO] (http://www.agirc-arrco.fr/documentation-multimedia/circulaires-2015/)
 - [ ] prélévements sociaux: ARRCO, AGIRC, AGFF, GMP, CET, APEC


## 1er avril

### Prestations sociales (TODO)


## 1er juillet

### Réunion du conseil d'administration de l'[AGS] (http://www.ags-garantie-salaires.org/archives.html) généralement doublée d'une [circulaire Unedic] (http://www.unedic.org/article/circulaires-de-l-unedic):
 - [ ] prélévements sociaux: AGS
 
### Revalorisation Aide au retour à l'emploi (ARE) (TODO)