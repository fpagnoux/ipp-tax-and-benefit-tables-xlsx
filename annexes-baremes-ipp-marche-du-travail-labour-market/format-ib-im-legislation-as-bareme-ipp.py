import argparse
import logging
import os
import pandas as pd
from pandas import ExcelWriter
import sys
import datetime as dt
import time
import xlsxwriter

app_name = os.path.splitext(os.path.basename(__file__))[0]
log = logging.getLogger(app_name)


asset_path = os.path.join(
    'Z:',
    '3-Legislation',
    'Baremes IPP',
    'annexes-baremes-ipp-marche-du-travail-labour-market'
    )

def main():
  parser = argparse.ArgumentParser(description = __doc__)
  parser.add_argument('-s', '--source-file', help = 'path of source XLS file', required = True)
  parser.add_argument('-v', '--verbose', action = 'store_true', default = False, help = u'Increase output verbosity')
  args = parser.parse_args()
  logging.basicConfig(level = logging.DEBUG if args.verbose else logging.WARNING, stream = sys.stdout)
  xls_path = args.source_file
  clean_and_save(xls_path)
 
def clean_and_save(xls_path):   
    df_ib_im = pd.read_excel(xls_path)
    df_ib_im_clean = pd.DataFrame()
    
    for ib in df_ib_im.ib.unique():
        df_by_ib = df_ib_im.loc[df_ib_im.ib == ib]
        df_by_ib_sorted = df_by_ib.sort_values(by='date', ascending=True)
        df_by_ib_sorted_no_dup = df_by_ib_sorted.drop_duplicates(['im'])
        df_by_ib_resorted_no_dup = df_by_ib_sorted_no_dup.sort_values(by='date', ascending=False)
        df_ib_im_clean = df_ib_im_clean.append(df_by_ib_resorted_no_dup)
    df_ib_im_clean.to_excel('IB-IM-2017.xlsx',
                            index = False)
 

if __name__ == '__main__':
    sys.exit(main())