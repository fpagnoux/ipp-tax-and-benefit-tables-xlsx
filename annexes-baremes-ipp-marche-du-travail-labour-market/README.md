## Actualiser le barème des correspondances entre IB et IM

Les valeurs des indices majorés (IM) correspondant aux indices bruts (IB)
de la fonction publique sont publiées par décret, deux fois par an environ.
A chaque réforme, l'ensemble des correspondances entre IB et IM est publié dans
un tableau mis en annexe du décret. (Comme il existe 916 IB et que l'ensemble des correspondances entre IB et IM est
publié, un tableau compte 916 lignes.)

Chaque réforme modifie uniquement un petit nombre d'IM. En particulier, sont réformés les IM correspondants aux IB du
bas de l'échelle afin que les traitements des fonctionnaires les moins bien payés suivent les augmentations du SMIC.

Afin de faciliter la lecture de ce barème et pour éviter d'inutiles répétitions,
on souhaite uniquement publier dans les barèmes IPP les correspondances
entre IB et IM qui ont été modifiés d'un décret à l'autre.

Pour ce faire :

1. Copier le nouveau barème depuis Légifrance et le stocker en actualisant le fichier.
Vérifier que le format des cellules est standard, qu'il n'y a pas de redondance ni d'espace
dans les cellules contenant les indices.
````
IB-IM-unclean.xlsx
````

2. Exécuter dans une console le script suivant
````
python format-ib-im-legislation-as-bareme-ipp.py -s IB-IM-unclean.xlsx
````

3. Copier le contenu du fichier suivant et le coller dans le barème du marché du travail
et formater les colonnes comme indiqué dans la documentation.
````
baremes_ipp_marche_du_travail_IBIM.xlsx
````
4. Et...améliorez le script en trouvant comment :
   - Formater automatiquement les colonnes du fichier excel de sortie (format des dates et couleurs notamment)
   - Ecrire directement le barème formaté dans la feuille ````IB-IM```` du barème marché du
   travail
